[![pipeline status](https://gitlab.com/afivan/mindgaze-tools-secrets/badges/master/pipeline.svg)](https://gitlab.com/afivan/mindgaze-tools-secrets/commits/master)

# Mindgaze.Tools.Secrets

This tool and library makes use of AES algorithm and helps the developer store application secrets in an encrypted way. 
In order to decrypt, the library will need to be initialized with the _KEY_ and _IV_ parameters. By storing them safely on the production environment, you'll get your secrets protected.

You can find a tutorial [here](https://blog.mindgaze.tech/2018/10/storing-secrets-in-net-core-applications/) and one [sample](https://github.com/andreiflaviusivan/mindgaze-secrets-sample)

## Installation

0. Install as a library from nuget `dotnet add package Mindgaze.Tools.Secrets`
0. Install the CLI tool by modifing the csproj file:

   ```xml
   <ItemGroup>
     <DotNetCliToolReference Include="Mindgaze.Tools.Secrets" Version="2.0.1" />
   </ItemGroup>
   ```

Verify the installation by running `dotnet secrets generateKeys`

You should see something like this in the console output: 
```
Key: 'nf3z+NDa3Hb0kK0axnZT3AT/f6X4K3Y9VgQuh2CmANU='
IV: '9uKO1XSfF5AfDX3SO4oScg=='
```

## The CLI tool 

The tool can be used to generate keys, encrypt or decrypt values.

To encrypt a value run
`dotnet secrets encrypt 'My secret value' -key 'nf3z+NDa3Hb0kK0axnZT3AT/f6X4K3Y9VgQuh2CmANU=' -iv '9uKO1XSfF5AfDX3SO4oScg=='`

Output will be *'U3P6QY50Dr3ggcMz+/HIiuXZnGxXpXJYm4j5lWVqyBQ='*

To decrypt a value run
`dotnet secrets decrypt 'U3P6QY50Dr3ggcMz+/HIiuXZnGxXpXJYm4j5lWVqyBQ=' -key 'nf3z+NDa3Hb0kK0axnZT3AT/f6X4K3Y9VgQuh2CmANU=' -iv '9uKO1XSfF5AfDX3SO4oScg=='`

Output will be *'My secret value'*

__NOTE__ Mind the fact that the order of parameters is important!

### Saving Key and IV in environment variables

You can also save decryption parameters in environment

`PS $ $env:ASPNETCORE_SECRETS_KEY='nf3z+NDa3Hb0kK0axnZT3AT/f6X4K3Y9VgQuh2CmANU='`

`PS $ $env:ASPNETCORE_SECRETS_IV='9uKO1XSfF5AfDX3SO4oScg=='`

Now you don't need to supply these parameters to the command line

`dotnet secrets decrypt 'U3P6QY50Dr3ggcMz+/HIiuXZnGxXpXJYm4j5lWVqyBQ='`

Output will be *'My secret value'*

## Use in code

```
AppSecrets appSecrets = new AppSecrets("nf3z+NDa3Hb0kK0axnZT3AT/f6X4K3Y9VgQuh2CmANU=", "9uKO1XSfF5AfDX3SO4oScg==");
var testString = "This is one test string to encrypt";
var encrypted = appSecrets.Encrypt(testString);
```

## Use in Asp.Net Core

You can easily configure the AppSecrets to be available as a service in Asp.Net Core. Just add this line to the _ConfigureServices_ method:

`ApplicationSecrets = services.AddSecrets(Configuration["SECRETS_KEY"], Configuration["SECRETS_IV"]);`

Make sure you have the corresponding configuration keys available
