﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Mindgaze.Tools.Secrets.Tests
{
    public class AppSecretsTests
    {
        [Fact]
        public void GenerateKeys()
        {
            // Arrange
            var keysPair = AppSecrets.GenerateEncryptionParams();
            // Act
            AppSecrets secrets = new AppSecrets(keysPair.EncryptionKey, keysPair.EncryptionIV);
            // Assert
            Assert.NotNull(secrets);
        }

        [Theory]
        [InlineData("very simple and basic string with just these !#@&^$#& special chars")]
        [InlineData("")]
        [InlineData("9876543012__")]
        [InlineData("0294nfh38cl jfk3")]
        [InlineData("eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiIzNTIxMGVkMC1mYmQxLTRlOTctOTc2MS0yNGMxOWM2Y2Q4OWEiLCJ1bmlxdWVfbmFtZSI6Iml2YW5zY2hpIiwiQXNwTmV0LklkZW50aXR5LlNlY3VyaXR5U3RhbXAiOiJhMTU2MjQ5OS1hZWY5LTQ3Y2MtOTQ2Yy1lNGZhYzc1ZmE2NDUiLCJuYmYiOjE0NjcwMzMzNTcsImV4cCI6MTQ2NzAzNjk1NywiaWF0IjoxNDY3MDMzMzU3LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjQ5OTkiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0In0.FIiKwOVWZ8jZRF2K1BlZFokpxkEVZbo-OZl0H7rz2IjnxsxLmIFvSKxlxaKoF4PFm6frSJgxCYyqKEiTdbRjxiwat7LOK5-rxOilCeFuXwvXOJ6FCy7MZtt7Wx8NWsvr05Fg7_6ZZRisiH2IOT-vmPTIR551Ab_zcVHip1gLMBDDQ4XCeqsMc7C3WWUBP5lWch-uNu-EunQ49tzHXMra557WmVQFmdIsTPzSKM_K3bat5d372jZpUrQEE8lztW0dRdagmnElUKs-tWRw11UahYrBLAytTExcpFQYSglzF5xpPTeR6VAZyi-M9tuYxG9HtFo1ZAavYT7ZyVXSvuh1Jw")]
        public void GenerateKeysEncryptDecrypt(string dataToEncrypt)
        {
            // Arrange
            var keysPair = AppSecrets.GenerateEncryptionParams();
            AppSecrets secrets = new AppSecrets(AppSecrets.GenerateEncryptionParams());
            // Act

            var encrypted = secrets.Encrypt(dataToEncrypt);
            var decrypted = secrets.Decrypt(encrypted);

            // Assert
            Assert.Equal(dataToEncrypt, decrypted);
        }
    }
}
