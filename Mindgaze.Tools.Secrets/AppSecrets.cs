﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace Mindgaze.Tools.Secrets
{
    public sealed class AppSecrets
    {
        private readonly String EncryptionKey, EncryptionIV;
        private readonly Aes Engine;
        
        public AppSecrets(String encryptionKey, string encryptionIV)
        {
            EncryptionKey = encryptionKey;
            EncryptionIV = encryptionIV;

            Engine = Aes.Create();
            Engine.Key = Convert.FromBase64String(encryptionKey);
            Engine.IV = Convert.FromBase64String(encryptionIV);
        }

        public AppSecrets(AppSecretsCryptoParams cryptoParams) : this(cryptoParams.EncryptionKey, cryptoParams.EncryptionIV)
        {
        }

        public string Encrypt(String value)
        {

            var dataToEncode = GetBytes(value);
            var dataEncrypted = new List<byte>();
            using (var cryptoStream = Engine.CreateEncryptor())
            {
                int nrChunks = dataToEncode.Length / cryptoStream.InputBlockSize;
                int nrChunksLeft = dataToEncode.Length % cryptoStream.InputBlockSize;
                for (int i = 0; i < nrChunks; i++)
                {
                    var buffer = new byte[cryptoStream.OutputBlockSize];
                    var transformed = cryptoStream.TransformBlock(dataToEncode, i * (cryptoStream.InputBlockSize), cryptoStream.InputBlockSize, buffer, 0);
                    Array.Resize(ref buffer, transformed);
                    dataEncrypted.AddRange(buffer);
                }
                // Final range
                dataEncrypted.AddRange(cryptoStream.TransformFinalBlock(dataToEncode, dataToEncode.Length - nrChunksLeft, nrChunksLeft));
            }

            return Convert.ToBase64String(dataEncrypted.ToArray());
        }

        public String Decrypt(String value)
        {
            var dataDecrypted = new List<byte>();
            var dataToDecode = Convert.FromBase64String(value);

            using (var decryptoStream = Engine.CreateDecryptor())
            {
                int nrChunks = dataToDecode.Length / decryptoStream.InputBlockSize;
                int nrChunksLeft = dataToDecode.Length % decryptoStream.InputBlockSize;
                for (int i = 0; i < nrChunks; i++)
                {
                    var buffer = new byte[decryptoStream.OutputBlockSize];
                    var transformed = decryptoStream.TransformBlock(dataToDecode, i * (decryptoStream.InputBlockSize), decryptoStream.InputBlockSize, buffer, 0);
                    Array.Resize(ref buffer, transformed);
                    dataDecrypted.AddRange(buffer);
                }
                // Final range
                dataDecrypted.AddRange(decryptoStream.TransformFinalBlock(dataToDecode, dataToDecode.Length - nrChunksLeft, nrChunksLeft));
            }
            
            return GetString(dataDecrypted.ToArray());
        }

        public static AppSecretsCryptoParams GenerateEncryptionParams()
        {
            using (var aes = Aes.Create())
            {
                return new AppSecretsCryptoParams()
                {
                    EncryptionKey = Convert.ToBase64String(aes.Key),
                    EncryptionIV = Convert.ToBase64String(aes.IV),
                };
            }
        }

        private static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        private static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
    }
}
