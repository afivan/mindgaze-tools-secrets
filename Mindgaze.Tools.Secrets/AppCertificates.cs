﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using Mindgaze.Tools.Secrets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Mindgaze.Tools.Secrets
{
    public class AppCertificates
    {
        private readonly IOptions<CertificatesConfiguration> CertificatesConfiguration;
        private readonly AppSecrets Secrets;
        private readonly IHostingEnvironment Environment;

        private readonly Dictionary<string, X509Certificate2> LoadedCertificates = new Dictionary<string, X509Certificate2>();

        public AppCertificates(IOptions<CertificatesConfiguration> certificatesConfiguration, AppSecrets secrets, IHostingEnvironment environment)
        {
            CertificatesConfiguration = certificatesConfiguration;
            Secrets = secrets;
            Environment = environment;

            LoadCertificates();
        }

        private void LoadCertificates()
        {
            foreach(var certName in CertificatesConfiguration.Value.Certificates.Keys)
            {
                var certDetails = CertificatesConfiguration.Value.Certificates[certName];

                LoadedCertificates.Add(certName, new X509Certificate2(Path.Combine(Environment.ContentRootPath, certDetails.Path), Secrets.Decrypt(certDetails.Pass)));
            }
        }

        public X509Certificate2 GetCertificate(string certName)
        {
            return LoadedCertificates[certName];
        }

        public X509Certificate2 this[string certName]
        {
            get
            {
                return GetCertificate(certName);
            }
        }
    }
}
