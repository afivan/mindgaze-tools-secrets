﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace Mindgaze.Tools.Secrets
{
    public class Program
    {
        private const String ASPNETCORE_SECRETS_KEY = "ASPNETCORE_SECRETS_KEY";
        private const String ASPNETCORE_SECRETS_IV = "ASPNETCORE_SECRETS_IV";

        private enum ActionToTake
        {
            Generate,
            Encrypt,
            Decrypt,
        }

        public static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("Usage: dotnet secrets generateKeys|encrypt|decrypt \"string-value\"");
                Console.WriteLine($"Encryption keys can be loaded from environment ({ASPNETCORE_SECRETS_KEY}, {ASPNETCORE_SECRETS_IV}) or command line -key 'value' and -iv 'value'. The commandline values will override the environment values");
                return;
            }

            string encryptionKey = Environment.GetEnvironmentVariable(ASPNETCORE_SECRETS_KEY);
            string encryptionIV = Environment.GetEnvironmentVariable(ASPNETCORE_SECRETS_IV);

            String value = String.Empty;

            var action = ActionToTake.Generate;

            for (int i = 0; i < args.Length; i++)
            {
                var arg = args[i];
                switch (arg)
                {
                    case "generateKeys":
                        action = ActionToTake.Generate;

                        break;

                    case "encrypt":
                        action = ActionToTake.Encrypt;

                        if (i < args.Length - 1)
                        {
                            value = args[i + 1];
                        }
                        else
                        {
                            Console.Error.WriteLine("Please supply a value for encryption!");

                            return;
                        }

                        break;

                    case "decrypt":
                        action = ActionToTake.Decrypt;

                        if (i < args.Length - 1)
                        {
                            value = args[i + 1];
                        }
                        else
                        {
                            Console.Error.WriteLine("Please supply a value for decryption!");

                            return;
                        }

                        break;

                    case "-key":

                        if (i < args.Length - 1)
                        {
                            encryptionKey = args[i + 1];
                        }
                        else
                        {
                            Console.Error.WriteLine("Please supply a value for the -key parameter!");

                            return;
                        }

                        break;

                    case "-iv":

                        if (i < args.Length - 1)
                        {
                            encryptionIV = args[i + 1];
                        }
                        else
                        {
                            Console.Error.WriteLine("Please supply a value for the -iv parameter!");

                            return;
                        }

                        break;

                    default:
                        break;
                }
            }

            // Do the action
            switch(action)
            {
                case ActionToTake.Generate:
                    var parameters = AppSecrets.GenerateEncryptionParams();

                    Console.WriteLine($"Key: '{parameters.EncryptionKey}'");
                    Console.WriteLine($"IV: '{parameters.EncryptionIV}'");

                    break;

                case ActionToTake.Encrypt:
                    if(!String.IsNullOrEmpty(encryptionKey) && !String.IsNullOrEmpty(encryptionIV))
                    {
                        AppSecrets secrets = new AppSecrets(encryptionKey, encryptionIV);

                        try
                        {
                            Console.WriteLine(secrets.Encrypt(value));
                        }
                        catch(CryptographicException cryptoEx)
                        {
                            Console.Error.WriteLine($"Could not encrypt data: {cryptoEx.Message}");
                        }
                        catch(Exception ex)
                        {
                            throw ex;
                        }
                    }
                    else
                    {
                        Console.Error.WriteLine("You need to provide values for -key and -iv either through environment variables or command line!");
                    }

                    break;

                case ActionToTake.Decrypt:
                    if (!String.IsNullOrEmpty(encryptionKey) && !String.IsNullOrEmpty(encryptionIV))
                    {
                        AppSecrets secrets = new AppSecrets(encryptionKey, encryptionIV);

                        try
                        {
                            Console.WriteLine(secrets.Decrypt(value));
                        }
                        catch (CryptographicException cryptoEx)
                        {
                            Console.Error.WriteLine($"Could not decrypt data: {cryptoEx.Message}");
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                    else
                    {
                        Console.Error.WriteLine("You need to provide values for -key and -iv either through environment variables or command line!");
                    }

                    break;
            }
        }
    }
}
