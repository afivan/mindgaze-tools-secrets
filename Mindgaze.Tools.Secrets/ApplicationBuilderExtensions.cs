﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace Mindgaze.Tools.Secrets
{
    public static class ApplicationBuilderExtensions
    {
        public static AppSecrets AddSecrets(this IServiceCollection services, string key, string iv)
        {
            var secrets = new AppSecrets(key, iv);

            services.AddSingleton(p => secrets);

            return secrets;
        }

        public static IServiceCollection AddCertificates(this IServiceCollection services, IConfiguration config)
        {
            services.AddOptions();
            services.Configure<CertificatesConfiguration>(config);

            services.AddSingleton<AppCertificates>();

            return services;
        }
    }
}
