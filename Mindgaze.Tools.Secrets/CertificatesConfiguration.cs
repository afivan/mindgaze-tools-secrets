﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.Tools.Secrets
{
    public class CertificatesConfiguration
    {
        public Dictionary<string, CertificateOptions> Certificates
        {
            get;set;
        }
    }
}
