﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.Tools.Secrets
{
    public struct AppSecretsCryptoParams
    {
        public string EncryptionKey;
        public string EncryptionIV;
    }
}
