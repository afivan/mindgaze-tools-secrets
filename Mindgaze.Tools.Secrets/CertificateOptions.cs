



namespace  Mindgaze.Tools.Secrets
{
    public class CertificateOptions
    {
        public string Path { get; set; }
        public string Pass { get; set; }
    }
}