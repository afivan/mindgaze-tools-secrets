﻿using Mindgaze.Tools.Secrets;
using System;

namespace MdzSecretsRunSample
{
    class Program
    {
        static void Main(string[] args)
        {
            AppSecrets appSecrets = new AppSecrets("nf3z+NDa3Hb0kK0axnZT3AT/f6X4K3Y9VgQuh2CmANU=", "9uKO1XSfF5AfDX3SO4oScg==");

            var testString = "This is one test string to encrypt";

            var encrypted = appSecrets.Encrypt(testString);

            Console.WriteLine($"Encrypted value is: {encrypted}");

            Console.WriteLine($"Decrypted value is: {appSecrets.Decrypt(encrypted)}");
        }
    }
}
